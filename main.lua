followers = 0

function begin()
	print "You are using Twitter\nYou can: Look At, Participate In or Ingore\nOutrage and Look At, Participate In, Or Ingore\nFunny Hashtag.\nyou can also Stop Using Twitter or \nask for Help (this message)\n"
	return main()
end

function main()
	print "\nYour timeline refreshes."
	local input = string.lower(io.read())
	if input == "help" then
		return begin()
	end
	if input == "stop using twitter" then
		print "I don't understand your command."
		return main()
	end

	local command, subject = input:match("^(.+)%s(.-)$")
	if subject == "hashtag" then
		command, subject = input:match("^(.+)%s(.-%s.-)$")
	end
	local identifier = subject == "outrage" and "an " or "a "
	local str
	if command == "look at" then
		str = "It is " .. identifier .. subject .. ". You are/are not offended."
	elseif command == "participate in" then
		str = "You participate in " .. subject .. '. You gain a follower.' 
		followers = followers + 1
	elseif command == "ignore" then
		str = "You ignore " .. subject .. ". You lose a follower."
		followers = followers - 1
	end
	print (str)
	return main()
end

begin()